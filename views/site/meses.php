<?php
    use yii\helpers\Html;
?>

<div class="card text-center">
    <div class="card-header text-bg-dark">
        <h5 class="card-title"> Meses</h5>
    </div>

    <div class="card-body">
        <?= 
            Html::ul($meses, [
                'class' => 'list-group list-group-flush', 
                "item" => function ($valor, $indice) {
                    if (!in_array($indice, ['emp_no', 'apellido', 'oficio']) && $valor) {
                        return "<li class='list-group-item list-group-item-action'>{$valor}</li>";
                    }
                }
            ]) 
        ?>
    </div>
</div>